$(function(){

    // ------------------------------------------
    // Verificación de si soporta geolocalización
    // ------------------------------------------
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getCoords, getError);
    } else {
        settingsmap(-25.363882, 131.044922);
    }

    // -----------------------------
    // Posicion actual
    // ------------------------------
    function getCoords(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;

        initialize(lat, lng);
    }

    function getError(err) {

        initialize(-25.363882, 131.044922);
    }


    function initialize(lat, lng) {
      var myLatlng = new google.maps.LatLng(lat, lng);
      var mapOptions = {
        zoom: 15,
        center: myLatlng
    }

    // ..................
    //
    // Creación del mapa
    //
    // ..................
    map = new google.maps.Map($('#map').get(0), mapOptions);

  var infowindow = new google.maps.InfoWindow({
    content: ' - -------------> Arrastrame: Para cambiar latitud y longitud',

  });
  infowindow.open(map);

      // -------------------------
      // Icono de localización
      // -------------------------
      var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          draggable: true,
          title: 'Arrastrarme'
      });


       // arrastrable
       google.maps.event.addDomListener(marker, 'position_changed',function() {
       getMarkerCoords(marker);


       // google.maps.event.addDomListener(window, 'load', initialize);
       // function initialize() {
       //  }
    });

  // .............................................
  //
  // Callback del arrastre del icono localización.
  //
  // ..............................................
  function getMarkerCoords (market){
    var markerCoords = marker.getPosition();

    // -----------------------------------------
    // Actuliza campos del formulario en la web.
    // -----------------------------------------
    $('#id_lat').val( markerCoords.lat() );
    $('#id_lng').val( markerCoords.lng() );
  }

  // .......................................................
  //
  // Se dispara cuando se da click al boton guardar cambios.
  // (de index.html utilizando el 'id' de '<form...'
  // y el 'type' de '<input...')
  //
  // .......................................................
  $('#form_coords').submit(function(e) {

    // --------------------------------------------
    // En lugar de enviar el formulario a la url...
    // --------------------------------------------
    e.preventDefault();

    // -----------------------------------------------------------------------------------
    // ...se hace el envio mediante ajax.
    //
    // => '/coords/save': url donde se envian los datos
    // => serialize()   : se envian todos los datos que estan en los campos del formulario)
    // => function(data)..., json: respuesta del servidor en formato json.

    // -----------------------------------------------------------------------------------
    $.post('/coords/save', $(this).serialize(), function(data){
        if (data.ok) {

            // ----------------------------------------------------------------
            // Muestra la información enviada por simplejson.dumps en views.py
            // en el div 'data' de gmap/index.html.
            // ----------------------------------------------------------------
            $('#data').html(data.msg);  //... renderiza como html las etiquetas

            // ----------------------------------------------
            // Limpiar el formulario recorriendo cada campo
            // ----------------------------------------------
            $('#form_coords').each(function(){ this.reset(); });

        } else {
            alert(data.msg);
        }

    },'json');
  });

    }



});