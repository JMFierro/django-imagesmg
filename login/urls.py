# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

urlpatterns = patterns('',
    # --------------------------------------------------------
    ## Enrutamiento (el uso de name me permite cambiar el
    ## nombre de las clases desde aquí sin que afecte a
    ## donde se usen)
    # ----------------------------------------------------
    url(r'^login/$','login.views.login_page', name='login'),
    url(r'^logout/$','login.views.logout_view', name='logout'),
)