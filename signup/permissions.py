# -*- coding: utf-8 -*-

from rest_framework import permissions
from django.core.validators import validate_email
# ---------------------------------------------
#
# IsOwnerOrReadOnly: permiso personalizado
# (permissions.py), para que sólo el usuario
#  que creo la imagen pueda modificarla o
# eliminarla.
#
# ---------------------------------------------
class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):

        """
        if request.PUT.get("email", "") == "":
            return False
        """

        try:
            validate_email(request.PUT.get("email", ""))
        except:
            return obj == request.user or request.user.is_staff

        """
        if not request.user.email == "":
            return False
        """



