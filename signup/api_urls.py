# -*- coding: utf-8 -*-

__author__ = 'jose.fierro'

from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter
#from api import UserViewSet
import api

#Crea router y registra viewset
#router = DefaultRouter()
#router.register(r'user', UserViewSet)

# publica URL para importar proyecto
urlpatterns = patterns('',
     #url(r'', include(router.urls))
     url(r'^signup/$', api.UserList.as_view()),
     #url(r'^signup/(?P<pk>[0-9]+)/$', api.UserDetail.as_view()),
     url(r'^signup/(?P<pk>[a-zA-Z0-9_.-]+)/$', api.UserDetail.as_view()),

)
