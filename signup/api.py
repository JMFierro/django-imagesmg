# -*- coding: utf-8 -*-

__author__ = 'jose.fierro'

from rest_framework.viewsets import ModelViewSet  # ReadOnlyModelViewSet
from serializers import UserSerializer
from django.contrib.auth.models import User

from rest_framework import permissions
from permissions import IsOwnerOrReadOnly
from rest_framework import generics, permissions

"""
class UserViewSet(ModelViewSet):  # ReadOnlyModelViewSet):

    queryset = User.objects.filter(is_active=True) # solo usuarios activos
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                      IsOwnerOrReadOnly,)

"""

class UserList(generics.ListCreateAPIView):
    #queryset = User.objects.all()
    serializer_class = UserSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly)


    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        #user_actual = self.request.user
        if self.request.user.is_staff:
            users = User.objects.all()
        else:
            users = User.objects.filter(username=self.request.user)

        return users



class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    #queryset = User.objects.all()
    serializer_class = UserSerializer

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly)

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        #user_actual = self.request.user
        if self.request.user.is_staff:
            users = User.objects.all()
        else:
            users = User.objects.filter(username=self.request.user)

        return users