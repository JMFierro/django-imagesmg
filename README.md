# JMFierro #
# Práctica Back-end y Front-end con Django, Máster de desarrollo de apps, U-Tad 2014 #

Para el súper usuario(staff=true) muestro la totalidad de las imágenes de todos los usuarios. Para el resto de los usuarios sólo las suyas.

Aunque en la práctica se pedía que se mostrará sólo el titulo y la url para el listado de imágenes, muestro también el ‘username’ y el campo ‘is_public’, con el fin de que sea más fácil la evaluación del ejercicio. En la web además incluyo la url para que se pueda hacer link.

# ESPECIFICACIONES #

 Servicio privado de almacenamiento de imágenes en la nube. Para ello, se 
deberá crear un API Rest que permita almacenar imágenes con los siguientes datos: 

*  URL de la imagen (donde estará alojada)
*  Título de la imagen
*  Descripción
*  Datos de geolocalización opcionales (latitud y longitud)
*  Fecha de creación
*  Fecha de última actualización
*  Un ﬂag para indicar si la imagen es pública o privada. Este ﬂag permitirá que otros usuarios que no son propietarios de la imagen puedan verla. 

## El API Rest deberá proporcionar los siguientes endpoints: ##
 
1 - Listado de imágenes públicas

*  Debe listar las imágenes clasiﬁcadas como públicas por los usuarios
*  Se mostrará el título y la URL de cada imagen
*  No será necesaria autenticación 

2 - Listado de imágenes de un usuario

*  Debe listar las imágenes del usuario que está autenticado
*  Se mostrará título, URL y si es pública o no
*  Será necesaria autenticación 

3 - Creación de una imagen

*  Permite crear una imagen enviando los datos: URL, titulo, descripción, datos de geolocalización y ﬂag de pública/privado.
*  Será necesaria autenticación 

4 - Detalle de una imagen

*  Muestra todos los campos de la imagen
*  La autenticación será opcional (ver siguiente punto)
*  Sólo el propietario de una imagen podrá ver el detalle de la imagen, salvo que ésta sea pública. 

5 - Actualización de una imagen

*  Permite actualizar una imagen enviando los datos: URL, titulo, descripción, datos de 
* geolocalización y ﬂag de pública/privado
*  Será necesaria autenticación
*  Sólo el propietario de la imagen o un usuario super administrador podrá actualizar los datos 
* Borrado de una imagen
*  Permite borrar una imagen Será necesaria autenticación
*  Sólo el propietario de la imagen o un usuario super administrador podrá eliminar la imagen

## Sitio web ##

Se desea crear un sitio web que muestre una sola página con los siguiente elementos: 

 Cuando el usuario no esté autenticado: 

*  Un formulario de login para iniciar sesión.
*  Un listado de las imágenes públicas. 
*  Cuando el usuario esté autenticado: 
*  Un enlace de logout para ﬁnalizar la sesión.
*  Un listado de imágenes que contenga el listado de imágenes públicas (que no contengan 
* imágenes del usuario autenticado)
*  Un listado de las imágenes del usuario. 

## Parte opcional: Endpoint para subida de imágenes ##

Crear un endpoint en el API Rest para permitir la subida de archivos de imagen, teniendo en 
cuenta las siguientes restricciones: 

* Los archivos de imagen deberán almacenarse en una carpeta del servidor
* Será necesaria autenticación para poder utilizar el endpoint
* Cada imagen, deberá quedar relacionada con el usuario que la crea
* Se deberá registrar la hora de creación de cada imagen
* Cuando se cree la imagen, se deberá devolver la URL como respuesta a la petición de creación (de esta manera, esta URL podría ser utilizada para enviar en el endpoint de almacenamiento creado como parte de la práctica).
* Las imágenes subidas, deberán estar accesibles a través de HTTP para poder verlas con el  navegador web (para más información: https://docs.djangoproject.com/en/dev/faq/usage/#how-do-i-use-image-and-ﬁle-ﬁelds) para que se puedan visualizar desde e sitio web.
* Para poder implementar este servicio, será necesario instalar el paquete PIL o Pillow en el entorno virtual. En OS X Mavericks puede haber problemas de instalación directa ya que es necesario tener instaladas las command line tools de Xcode. Una posible solución es, desde la consola, modiﬁcar las variables globales CFLAGS y CPPFLAGS y posteriormente ejecutar de nuevo pip install Pillow (ver http://stackoverﬂow.com/a/22351527 para más información). 
*Parte opcional: Endpoint para registro de usuario


Crear un endpoint en el API Rest para permitir el registro de un usuario recibiendo los datos:
 
*  Nombre
*  Apellidos
*  Nombre de usuario
*  E-mail
*  Contraseña
*  Conﬁrmación de contraseña 


 Restricciones: 

*  Todos los campos son obligatorios, ninguno puede ser vacío. Se deberá que no existe ningún usuario con el mismo nombre de usuario o e-mail en el sistema  para permitir realizar el registro.
*  Deberá validarse que el e-mail tiene un formato correcto.
*  Deberá validarse que la contraseña enviada y y la conﬁrmación son iguales.