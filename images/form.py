from django import forms
from images.models import Image


class ImageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['url', 'title', 'description', 'address', 'lat', 'lng', 'is_public']
