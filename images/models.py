# -*- coding: utf-8 -*-

from django.db import models


# Create your models here.
class Image(models.Model):

    username = models.ForeignKey('auth.User', related_name='snippets')

    url = models.CharField(max_length=100)
    title = models.CharField(max_length=50)
    description = models.TextField()

    address = models.CharField(max_length=100)
    lat = models.FloatField(null=True)
    lng = models.FloatField(null=True)

    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now_add=True)
    is_public = models.BooleanField("¿La imagen es pública?", default=True)


    def __unicode__(self):
        return self.title

    def isPublic(self):
        return self.public



# -------------------------------------------------
# Formulario para mostrar y editar datos en la web
# --------------------------------------------------
#class PictureForm(ModelForm):
#    class Meta:
#        model = Picture