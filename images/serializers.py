# -*- coding: utf-8 -*-

from models import Image
from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

class UserSerializer(serializers.ModelSerializer):
    snippets = serializers.PrimaryKeyRelatedField(many=True)

    class Meta:
        model = User
        fields = ('id', 'username','snippets')

class ImageSerializer(serializers.ModelSerializer):
    # para que actulize el atributo con el usuario actual
    username = serializers.Field(source='username')

    class Meta:
        model = Image
        #filter(is_public=False)


        fields = ('username', 'title', 'url','is_public',)



class ImageDetailSerializer(ImageSerializer):
    username = serializers.Field(source='username')

    class Meta(ImageSerializer.Meta):
            fields = None

    """

    def restore_object(self, attrs, instance=None):
        # call set_password on user object. Without this
        # the password will be stored in plain text.
        images = super(ImageSerializer, self).restore_object(attrs, instance)
        return images

    """