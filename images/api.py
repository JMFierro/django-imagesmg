# -*- coding: utf-8 -*-

from models import Image
from serializers import ImageSerializer, ImageDetailSerializer, UserSerializer
from rest_framework import generics, permissions
from permissions import IsOwnerOrReadOnly


# ................
#
#  API
#
# .................
class ImageList(generics.ListCreateAPIView):
    #queryset = Image.objects.all()
    serializer_class = ImageSerializer
    # ------------------------------------------
    #  IsAuthenticatedOrReadOnly:
    #    Sólo al usuario autentificado se le permite
    # crear, actualizar y borrar imagenes.
    # -------------------------------------------
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        #user_actual = self.request.user
        if self.request.user.is_staff:
            images = Image.objects.all()
        elif self.request.user.is_authenticated():
            images = Image.objects.filter(username=self.request.user)
        else:
            images = Image.objects.filter(is_public=True)

        return images

    # guarda el usuario actual
    def pre_save(self, obj):
        obj.username = self.request.user

class ImageDetail(generics.RetrieveUpdateDestroyAPIView):
    #queryset = Image.objects.all()
    serializer_class = ImageDetailSerializer
    # ---------------------------------------------
    #  IsAuthenticatedOrReadOnly:
    #    Sólo al usuario autentificado se le permite
    # crear, actualizar y borrar imagenes.

    # IsOwnerOrReadOnly: permiso personalizado
    # (permissions.py), para que sólo el usuario
    #  que creo la imagen pueda modificarla o
    # eliminarla.
    # ---------------------------------------------
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                      IsOwnerOrReadOnly,)

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        #user_actual = self.request.user
        if self.request.user.is_staff:
            images = Image.objects.all()
        elif self.request.user.is_authenticated():
            images = Image.objects.filter(username=self.request.user)
        else:
            images = Image.objects.filter(is_public=True)

        return images

    # guarda el usuario actual
    def pre_save(self, obj):
        obj.username = self.request.user
