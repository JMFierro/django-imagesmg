# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
import api

urlpatterns = patterns('',
    # API
    url(r'^images/$', api.ImageList.as_view()),
    url(r'^images/(?P<pk>[0-9]+)/$', api.ImageDetail.as_view()),
)

urlpatterns = format_suffix_patterns(urlpatterns)