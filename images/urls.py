# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from images import views

urlpatterns = patterns('',
    # --------------------------------------------------------
    ## Enrutamiento (el uso de name me permite cambiar el
    ## nombre de las clases desde aquí sin que afecte a
    ## donde se usen)
    # ----------------------------------------------------
    url(r'^images/$','images.views.images_list', name='images_list'),
    url(r'^images/(?P<image_id>\d+)/$','images.views.image_detail', name='image_detail'),

    #url(r'^images/lista/$','images.views.images_lista', name='images_lista'),
    url(r'^images/create/$','images.views.image_create', name='image_create'),
    url(r'^images/edit/(?P<image_id>\d+)/$','images.views.image_edit', name='image_edit'),
)

urlpatterns = format_suffix_patterns(urlpatterns)