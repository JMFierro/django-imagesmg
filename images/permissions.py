# -*- coding: utf-8 -*-

from rest_framework import permissions

# ---------------------------------------------
#
# IsOwnerOrReadOnly: permiso personalizado
# (permissions.py), para que sólo el usuario
#  que creo la imagen pueda modificarla o
# eliminarla.
#
# ---------------------------------------------
class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.


        # Write permissions are only allowed to the owner of the snippet.
        return obj.username == request.user or request.user.is_staff
