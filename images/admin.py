# -*- coding: utf-8 -*-

from django.contrib import admin
from models import Image

class ImageAdmin(admin.ModelAdmin):

    list_display = ('title', 'address', 'date_update','is_public')
    list_filter = ('address', 'date_creation')
    search_fields = ('title', 'address')

    fieldsets = (
        ('Usuario', {
            'fields' : ('user',)
        }),
        ('Titulo de la imagen', {
            'fields' : ('title',)
        }),
        ('Direccion y descripción', {
           'fields' : ('address', 'description','is_public','url'),
           'classes' : ('wide',)
        })
    )


# Register your models here.
admin.site.register(Image, ImageAdmin)

