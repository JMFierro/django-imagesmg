# -*- coding: utf-8 -*-

from django.http import Http404, HttpResponse
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.utils import timezone
from django.contrib.auth.decorators import login_required

from models import Image
from form import ImageForm



# .........................................
#
# Las view son funciones python,
#   que aceptan un objeto resquest,
#   y devuelven un objeto HttpResponse.
#
# Django
# Las peticiones del usuario (solicitud de una pagina) django realiza
# los siguientes pasos:
#
#   - mira setting.py, busca ROOT_URLCONG que indica cual
#   el modulo de enrutamiento (ursl.py).
#
#   - en 'urls.py' mira la variable 'urlpatterns', que
#   contiene una serie de patters
#   con objetos (url, Python callback),
#   buscando la url de la petición y llama a
#   la funcion correspondiente en 'views.py'.
#
# .........................................
def images_list(request):
    #images = Image.objects.all()

    if request.user.is_staff:
        images = Image.objects.all()
    elif request.user.is_authenticated():
        images = Image.objects.filter(username=request.user)
    else:
        images = Image.objects.filter(is_public=True)

    # ---------------------------------------------------
    # Para no mostrar los datos desde views.py,
    # respetando el modelo de MTV, se usan los templates
    # --------------------------------------------------
    #respuesta_string = "Imagenes <br/>"
    #respuesta_string += '<br/>'.join(["id: %s, title: %s, url: %s"%
    #                    (image.id, image.title, image.url) for image in images])
    #return HttpResponse(respuesta_string)

    # --------------------------------------------------------
    # Plantilla a renderizar images/images_list.html.
    # Cada vista se relaciona con una url (url.py)
    #
    # -> form: envio del formulario
    # -> Con RequestContext se pasa información  de
    # la petición a las plantillas (dicionario python).
    # --------------------------------------------------------

    #user = User.objects.all()
    #return render_to_response('images/images_list.html',{'images':images, 'user':user})
    return render_to_response('images/images_list.html',
                          {'images':images},
                          context_instance=RequestContext(request))




def image_detail(request, image_id):

    # Manager item find/not find
    try:
        image = Image.objects.get(pk=image_id)
    except Image.DoesNotExist:
#        return HttpResponse("%s %s" % image_id, "no existe", status=404)
#        return HttpResponse(status=404)
        raise Http404

    #user = User.objects.all()
    #return render_to_response('images/image_detail.html',{'image':image, 'user':user})
    return render_to_response('images/image_detail.html',
                          {'image':image},
                          context_instance=RequestContext(request))




# ------------------------------------------------------
# pasa el formulario a la plantilla,
# -> context_instance=... pasa todas las variables a la
# plantilla  para poder acceder desde ella.
# ------------------------------------------------------
@login_required()
def image_create(request):
    if request.method == 'POST':
        form = ImageForm(request.POST)
        if form.is_valid():
            image = Image(username = request.user,
                          url = form.cleaned_data['url'],
                          title = form.cleaned_data['title'],
                          description = form.cleaned_data['description'],
                          address = form.cleaned_data['address'],
                          lat = form.cleaned_data['lat'],
                          lng = form.cleaned_data['lng'],
                          date_creation = timezone.now(),
                          date_update = timezone.now(),
                          is_public = form.cleaned_data['is_public']
                          )
            image.save()
            return redirect('images_list')
    else:
        form = ImageForm()
    return render_to_response('images/image_create.html',
                              {'form':form},
                              context_instance=RequestContext(request))


@login_required()
def image_edit(request, image_id):
    # Manager item find/not find
    try:
        image = Image.objects.get(pk=image_id)
    except Image.DoesNotExist:
#        return HttpResponse("%s %s" % image_id, "no existe", status=404)
#        return HttpResponse(status=404)
        raise Http404
    user = User.objects.all()

    if request.method == 'POST':
        form = ImageForm(request.POST, instance=image)
        if form.is_valid():
            form.save()
            return redirect('image_detail', image_id)
    else:
        form = ImageForm(instance=image)
    return render_to_response('images/image_edit.html',
                              {'form':form},
                              context_instance=RequestContext(request))



