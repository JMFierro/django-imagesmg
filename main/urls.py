# -*- coding: utf-8 -*-

from django.conf.urls import include
from rest_framework import routers
from django.conf.urls import patterns, url

from django.contrib import admin
admin.autodiscover()

#router = routers.DefaultRouter()
#router.register(r'signup', views.UserViewSet)
#router.register(r'groups', views.GroupViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'main.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # -----------------
    # Pagina de inicio
    # -----------------
    url(r'^$','main.views.homepage', name='homepage'),

    url(r'^', include('login.urls')),
    url(r'^register/$', 'signin.views.register', name='register'),

    # ---------------------------
    # Interface de administación
    # nuevo usuario: python manage.py createsuperuser --username=admin --email=admin@admin.com
    # ---------------------------
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),


    # ------------------------------------------------
    # url para salvar base de datos de Django.
    #  Aquí se une el html donde esta el formulario(coords/save)
    # y la funcion en views.py (coords_save)
    # (hay que crear la vista name='coods_save' en view.py
    # y en el html mediante la instrucción <form action="...
    # se indica la url a llamar: coords/save)
    # ------------------------------------------------
    #url(r'^coords/save', 'images.views.coords_save', name='coords_save'),

    # API
    url(r'^', include('images.urls')),
    url(r'^api/1.0/', include('images.api_urls')),
    url(r'^api/1.0/', include('signup.api_urls')),
)

