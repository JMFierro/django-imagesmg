# -*- coding: utf-8 -*-

from models import UserProfile, models
from django.contrib.auth.models import User
from django import forms

class UserForm(forms.ModelForm):
    #password = forms.CharField(widget=forms.PasswordInput())
    #confirm_password = forms.CharField(widget=forms.PasswordInput())
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        '''Required custom validation for the form.'''
        super(forms.ModelForm,self).clean()
        if 'password' in self.cleaned_data and 'confirm_password' in self.cleaned_data:
            if self.cleaned_data['password'] != self.cleaned_data['confirm_password']:
                self._errors['password'] = [u'Passwords y Confirm password deben de coincidir']
                #self._errors['confirm_password'] = [u'Passwords must match.']
        return self.cleaned_data

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password')
        widgets = {
            'password': forms.PasswordInput(),
        }

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        #fields = ('website', 'picture')



class PasswordField(forms.CharField):
    widget = forms.PasswordInput

class PasswordModelField(models.CharField):

    def formfield(self, **kwargs):
        defaults = {'form_class': PasswordField}
        defaults.update(kwargs)
        return super(PasswordModelField, self).formfield(**defaults)


